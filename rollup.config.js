import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import babel from 'rollup-plugin-babel';
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";

const packageJson = require("./package.json");

export default {
    input: "src/index.js",
    output: [
        {
            file: packageJson.main,
            format: "amd",
            sourcemap: true,
        }
    ],
    plugins: [
        peerDepsExternal(),
        babel(),
        resolve(),
        commonjs(),
    ]
}