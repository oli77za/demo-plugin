import React from "react";
import {DemoProvider, Page, SubPage, Widget} from "./DemoPlugin.js";
import OutdoorGrill from '@material-ui/icons/OutdoorGrill';
import OfflineBolt from '@material-ui/icons/OfflineBolt';
import wmcSdk from '@mwp/wmc-sdk';

wmcSdk.registerPlugin({
    id: 'demo-plugin',
    provider: DemoProvider,
    routes: {
        '/': Page,
        '/subpage': SubPage
    },
    widgets: [
        () => <Widget />,
        () => <Widget showButton />
    ],
    menus: [
        {
            title: 'Demo',
            path: '/',
            icon: <OutdoorGrill />,
        },
        {
            title: 'Demo - subpage',
            path: '/subpage',
            icon: <OfflineBolt />,
        }]
});
