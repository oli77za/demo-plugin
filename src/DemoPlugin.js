import React, {useContext, createContext, useState} from 'react';
import { Box, Button, Paper } from '@material-ui/core';
import {WidgetView} from '@mwp/frontend-components';
import wmcSdk from '@mwp/wmc-sdk';

export const DemoContext = createContext({});

export const DemoProvider = ({children}) => {
    const [count, setCount] = useState(0);
    const increase = () => setCount((prevCount) => prevCount + 1);
    return (
        <DemoContext.Provider value={
            { count, increase }
        }>
            {children}
        </DemoContext.Provider>
    )
};

export const Page = () =>  {
    const { count, increase } = useContext(DemoContext);
    const user = wmcSdk.getUser();
    return (
        <Box>
            <Paper m={1} p={2}>
                <h1>{`Hello ${user.email}`}</h1>
                <p>Your current count is <strong>{count}</strong></p>
            </Paper>
            <Button variant={"outlined"} onClick={increase}>Increase</Button>
            <Button 
              variant="outlined"
              onClick={() => wmcSdk.addMessage({text: 'You clicked the button', severity: 'info', autoHideDuration: 2000})}
            >
                Send message
            </Button>
        </Box>
    );
}

export const SubPage = () => <Box><Paper><h1>This is a sub-page</h1></Paper></Box>;

export const Widget = ({ showButton }) => {
    const { count, increase } = useContext(DemoContext);
    return (
        <WidgetView title='Demo plugin'>
            <Box p={1}>
                <h1>{`Current count ${count}`}</h1>
            </Box>
            { showButton && <Button onClick={increase}>Increase</Button> }
        </WidgetView>
    );
}

